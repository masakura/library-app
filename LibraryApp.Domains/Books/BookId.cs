﻿using System;

namespace LibraryApp.Domains.Books
{
    public sealed class BookId
    {
        private readonly Guid _value;

        public BookId(Guid value)
        {
            _value = value;
        }

        public static BookId NewId()
        {
            return new BookId(Guid.NewGuid());
        }
    }
}