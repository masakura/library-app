﻿namespace LibraryApp.Domains.Books
{
    public sealed class BookTitle
    {
        private readonly string _value;

        public BookTitle(string value)
        {
            _value = value;
        }
    }
}