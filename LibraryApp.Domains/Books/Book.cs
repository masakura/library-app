﻿namespace LibraryApp.Domains.Books
{
    public sealed class Book
    {
        public Book(BookId id, BookTitle title)
        {
            Id = id;
            Title = title;
        }

        public BookId Id { get; }
        public BookTitle Title { get; }

        public static Book NewBook(BookTitle title)
        {
            return new Book(BookId.NewId(), title);
        }
    }
}