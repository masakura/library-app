﻿using LibraryApp.Domains.Transactions.Lending;

namespace LibraryApp.Domains.Transactions.Returning
{
    internal sealed class ReturnedState : ILendState
    {
        private ReturnedState()
        {
        }

        public static ILendState Instance { get; } = new ReturnedState();

        public override string ToString()
        {
            return "返却済";
        }
    }
}