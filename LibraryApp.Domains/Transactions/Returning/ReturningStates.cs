﻿using LibraryApp.Domains.Transactions.Lending;

namespace LibraryApp.Domains.Transactions.Returning
{
    public static class ReturningStates
    {
        public static ILendState Returned { get; } = ReturnedState.Instance;
    }
}