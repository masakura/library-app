﻿using LibraryApp.Domains.Dates;

namespace LibraryApp.Domains.Transactions.Returning
{
    public class ReturnDate
    {
        private readonly Date _value;

        public ReturnDate(int year, int month, int day) : this(new Date(year, month, day))
        {
        }

        private ReturnDate(Date value)
        {
            _value = value;
        }
    }
}