﻿namespace LibraryApp.Domains.Transactions.Lending
{
    public static class LendingStates
    {
        public static ILendState Lending { get; } = LendingState.Instance;
        public static ILendState Overdue { get; } = OverdueState.Instance;
    }
}