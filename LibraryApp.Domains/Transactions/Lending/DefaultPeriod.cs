﻿using LibraryApp.Domains.Dates;

namespace LibraryApp.Domains.Transactions.Lending
{
    public static class DefaultPeriod
    {
        public static DatePeriod Default { get; } = new DatePeriod(7, "days");
    }
}