﻿using System;
using LibraryApp.Domains.Dates;

namespace LibraryApp.Domains.Transactions.Lending
{
    public sealed class DueDate : IEquatable<DueDate>
    {
        public DueDate(int year, int month, int day) : this(new Date(year, month, day))
        {
        }

        internal DueDate(Date value)
        {
            Value = value;
        }

        internal Date Value { get; }

        public bool Equals(DueDate other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(Value, other.Value);
        }

        public override string ToString()
        {
            return $"{Value}";
        }

        public override bool Equals(object obj)
        {
            return ReferenceEquals(this, obj) || obj is DueDate other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Value != null ? Value.GetHashCode() : 0;
        }

        public bool IsOverdue(Date today)
        {
            return today.GreaterThan(Value);
        }
    }
}