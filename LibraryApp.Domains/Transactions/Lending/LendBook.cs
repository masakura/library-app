﻿using LibraryApp.Domains.Books;
using LibraryApp.Domains.Users;

namespace LibraryApp.Domains.Transactions.Lending
{
    /// <summary>
    ///     書籍の貸し出し。
    /// </summary>
    public sealed class LendBook
    {
        private readonly BookId _bookId;
        private readonly LendDate _lendDate;
        private readonly LendPeriod _lendPeriod;
        private readonly UserId _userId;

        public LendBook(LendId id, UserId userId, BookId bookId, LendDate lendDate, LendPeriod lendPeriod)
        {
            Id = id;
            _userId = userId;
            _bookId = bookId;
            _lendDate = lendDate;
            _lendPeriod = lendPeriod;
        }

        public LendId Id { get; }

        public static LendBook NewLend(UserId userId, BookId bookId, LendDate lendDate)
        {
            return new LendBook(LendId.NewId(), userId, bookId, lendDate, LendPeriod.Default);
        }

        public DueDate DueDate()
        {
            return _lendDate.DueDate(_lendPeriod);
        }
    }
}