﻿namespace LibraryApp.Domains.Transactions.Lending
{
    internal class OverdueState : ILendState
    {
        private OverdueState()
        {
        }

        public static ILendState Instance { get; } = new OverdueState();

        public override string ToString()
        {
            return "期限超過";
        }
    }
}