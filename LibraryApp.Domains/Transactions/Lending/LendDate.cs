﻿using LibraryApp.Domains.Dates;

namespace LibraryApp.Domains.Transactions.Lending
{
    public sealed class LendDate
    {
        private readonly Date _value;

        public LendDate(int year, int month, int day) : this(new Date(year, month, day))
        {
        }

        private LendDate(Date value)
        {
            _value = value;
        }

        public DueDate DueDate(LendPeriod period)
        {
            return new DueDate(_value.Add(period.Value));
        }
    }
}