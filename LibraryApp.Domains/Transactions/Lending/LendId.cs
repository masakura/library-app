﻿using System;

namespace LibraryApp.Domains.Transactions.Lending
{
    public sealed class LendId
    {
        private readonly Guid _value;

        public LendId(Guid value)
        {
            _value = value;
        }

        public static LendId NewId()
        {
            return new LendId(Guid.NewGuid());
        }
    }
}