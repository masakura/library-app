﻿using System;
using LibraryApp.Domains.Dates;

namespace LibraryApp.Domains.Transactions.Lending
{
    /// <summary>
    ///     貸出期間。
    /// </summary>
    public sealed class LendPeriod
    {
        private LendPeriod(DatePeriod value)
        {
            Value = value;
        }

        public DatePeriod Value { get; }

        public static LendPeriod Default { get; } = new LendPeriod(DefaultPeriod.Default);
    }
}