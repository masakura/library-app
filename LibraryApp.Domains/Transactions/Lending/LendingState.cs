﻿namespace LibraryApp.Domains.Transactions.Lending
{
    /// <summary>
    ///     貸出中。
    /// </summary>
    internal sealed class LendingState : ILendState
    {
        private LendingState()
        {
        }

        public static ILendState Instance { get; } = new LendingState();

        public override string ToString()
        {
            return "貸出中";
        }
    }
}