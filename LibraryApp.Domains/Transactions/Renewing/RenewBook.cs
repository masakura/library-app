﻿using LibraryApp.Domains.Transactions.Lending;

namespace LibraryApp.Domains.Transactions.Renewing
{
    public sealed class RenewBook
    {
        private readonly LendId _lendId;
        private readonly RenewPeriod _period;

        private RenewBook(LendId lendId, RenewPeriod period)
        {
            _lendId = lendId;
            _period = period;
        }

        public static RenewBook NewRenew(LendId lendId)
        {
            return new RenewBook(lendId, RenewPeriod.Default);
        }

        public DueDate Extend(DueDate dueDate)
        {
            return new DueDate(dueDate.Value.Add(_period.Value));
        }
    }
}