﻿using System.Collections.Generic;
using System.Linq;
using LibraryApp.Domains.Transactions.Lending;

namespace LibraryApp.Domains.Transactions.Renewing
{
    public sealed class RenewBookCollection
    {
        private readonly IEnumerable<RenewBook> _items;

        public RenewBookCollection(IEnumerable<RenewBook> items)
        {
            _items = items;
        }

        public DueDate Extend(DueDate dueDate)
        {
            return _items.Aggregate(dueDate, (current, item) => item.Extend(dueDate));
        }
    }
}