﻿using LibraryApp.Domains.Dates;
using LibraryApp.Domains.Transactions.Lending;

namespace LibraryApp.Domains.Transactions.Renewing
{
    public sealed class RenewPeriod
    {
        public RenewPeriod(DatePeriod value)
        {
            Value = value;
        }

        internal DatePeriod Value { get; }

        public static RenewPeriod Default { get; } = new RenewPeriod(DefaultPeriod.Default);
    }
}