﻿using System;
using System.Collections.Generic;
using System.Linq;
using LibraryApp.Domains.Books;
using LibraryApp.Domains.Dates;
using LibraryApp.Domains.Transactions.Lending;
using LibraryApp.Domains.Transactions.Renewing;
using LibraryApp.Domains.Transactions.Returning;
using LibraryApp.Domains.Users;

namespace LibraryApp.Domains.Transactions
{
    public sealed class BookTransaction
    {
        private readonly Book _book;
        private readonly LendBook _lend;
        private readonly RenewBookCollection _renews;
        private readonly ReturnBook _return;
        private readonly User _user;

        public BookTransaction(User user, Book book, LendBook lend) :
            this(user, book, lend, Enumerable.Empty<RenewBook>())
        {
        }

        public BookTransaction(User user, Book book, LendBook lend, IEnumerable<RenewBook> renews) :
            this(user, book, lend, renews, null)
        {
        }

        public BookTransaction(User user, Book book, LendBook lend, IEnumerable<RenewBook> renews, ReturnBook @return)
        {
            _user = user;
            _book = book;
            _lend = lend;
            _renews = new RenewBookCollection(renews);
            _return = @return;
        }

        public static BookTransaction Rend(User user, Book book, LendDate lendDate)
        {
            throw new NotImplementedException();
        }

        public DueDate DueDate()
        {
            var firstDueDate = _lend.DueDate();
            var renewDueDate = _renews.Extend(firstDueDate);
            return renewDueDate;
        }

        public ILendState State()
        {
            return State(Date.Today());
        }

        public ILendState State(Date today)
        {
            // TODO null はよくないかも
            if (_return != null) return ReturningStates.Returned;
            if (DueDate().IsOverdue(today)) return LendingStates.Overdue;

            return LendingStates.Lending;
        }
    }
}