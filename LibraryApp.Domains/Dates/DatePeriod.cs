﻿namespace LibraryApp.Domains.Dates
{
    public sealed class DatePeriod
    {
        private readonly int _quantity;
        private readonly string _unit;

        public DatePeriod(int quantity, string unit)
        {
            _quantity = quantity;
            _unit = unit;
        }

        public Date AddTo(Date value)
        {
            return new Date(value.Value.AddDays(_quantity));
        }
    }
}