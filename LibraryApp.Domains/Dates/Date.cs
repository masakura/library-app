﻿using System;

namespace LibraryApp.Domains.Dates
{
    public sealed class Date : IEquatable<Date>
    {
        public Date(int year, int month, int day) : this(new DateTime(year, month, day))
        {
        }

        internal Date(DateTime value)
        {
            Value = value;
        }

        internal DateTime Value { get; }

        public bool Equals(Date other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Value.Equals(other.Value);
        }

        public Date Add(DatePeriod period)
        {
            return period.AddTo(this);
        }

        public override string ToString()
        {
            return $"{Value}";
        }

        public override bool Equals(object obj)
        {
            return ReferenceEquals(this, obj) || obj is Date other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public static Date Today()
        {
            return new Date(DateTime.Today);
        }

        public bool GreaterThan(Date value)
        {
            return Value > value.Value;
        }
    }
}