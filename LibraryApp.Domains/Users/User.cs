﻿namespace LibraryApp.Domains.Users
{
    public sealed class User
    {
        public User(UserId id, UserName name)
        {
            Id = id;
            Name = name;
        }

        public UserId Id { get; }
        public UserName Name { get; }

        public static User NewUser(UserName name)
        {
            return new User(UserId.NewId(), name);
        }
    }
}