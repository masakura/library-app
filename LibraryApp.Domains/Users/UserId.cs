﻿using System;

namespace LibraryApp.Domains.Users
{
    public sealed class UserId
    {
        private readonly Guid _value;

        public UserId(Guid value)
        {
            _value = value;
        }

        public static UserId NewId()
        {
            return new UserId(Guid.NewGuid());
        }
    }
}