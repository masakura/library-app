﻿using System.Linq;
using LibraryApp.Domains.Books;
using LibraryApp.Domains.Dates;
using LibraryApp.Domains.Transactions.Lending;
using LibraryApp.Domains.Transactions.Renewing;
using LibraryApp.Domains.Transactions.Returning;
using LibraryApp.Domains.Users;
using Xunit;

namespace LibraryApp.Domains.Transactions
{
    public sealed class BookTransactionTest
    {
        public BookTransactionTest()
        {
            _user = User.NewUser(new UserName("太郎", "山田"));
            _book = Book.NewBook(new BookTitle("現場で役立つシステム設計の原則"));
        }

        private readonly User _user;
        private readonly Book _book;

        private static BookTransaction CreateLend(User user, Book book)
        {
            var lend = LendBook.NewLend(user.Id, book.Id, new LendDate(2015, 3, 10));

            return new BookTransaction(user, book, lend);
        }

        private static BookTransaction CreateRenew(User user, Book book, int renewTimes)
        {
            var lend = LendBook.NewLend(user.Id, book.Id, new LendDate(2015, 3, 10));
            var renews = Enumerable.Range(0, renewTimes).Select(_ => RenewBook.NewRenew(lend.Id));
            return new BookTransaction(user, book, lend, renews);
        }

        private static BookTransaction CreateReturn(User user, Book book)
        {
            var lend = LendBook.NewLend(user.Id, book.Id, new LendDate(2015, 3, 10));
            var renews = Enumerable.Empty<RenewBook>();
            var @return = new ReturnBook(lend.Id, new ReturnDate(2015, 3, 15));

            return new BookTransaction(user, book, lend, renews, @return);
        }

        [Fact]
        public void 一週間後の当日まではステータスは貸し出し中()
        {
            var transaction = CreateLend(_user, _book);

            Assert.Equal(transaction.State(new Date(2015, 3, 17)), LendingStates.Lending);
        }

        [Fact]
        public void 貸し出した本のステータスは貸し出し中()
        {
            var transaction = CreateLend(_user, _book);

            Assert.Equal(transaction.State(new Date(2015, 3, 10)), LendingStates.Lending);
        }

        [Fact]
        public void 貸し出した本の返却予定日は一週間後()
        {
            var transaction = CreateLend(_user, _book);

            Assert.Equal(transaction.DueDate(), new DueDate(2015, 3, 17));
        }

        [Fact]
        public void 貸し出しを延長したけど返却予定日が過ぎた()
        {
            var transaction = CreateRenew(_user, _book, 2);

            Assert.Equal(transaction.State(new Date(2015, 3, 25)), LendingStates.Overdue);
        }

        [Fact]
        public void 貸し出しを延長した分返却予定日が伸びている()
        {
            var transaction = CreateRenew(_user, _book, 2);

            Assert.Equal(transaction.State(new Date(2015, 3, 24)), LendingStates.Lending);
        }

        [Fact]
        public void 貸し出しを延長する()
        {
            var transaction = CreateRenew(_user, _book, 2);

            Assert.Equal(transaction.DueDate(), new DueDate(2015, 3, 24));
        }

        [Fact]
        public void 返却されたらステータスは返却済み()
        {
            var transaction = CreateReturn(_user, _book);

            Assert.Equal(transaction.State(), ReturningStates.Returned);
        }

        [Fact]
        public void 返却予定日の翌日から期限超過()
        {
            var transaction = CreateLend(_user, _book);

            Assert.Equal(transaction.State(new Date(2015, 3, 18)), LendingStates.Overdue);
        }
    }
}